@extends('master')
<style>
#GS {
    -webkit-animation-duration: 4s;
    -webkit-animation-delay: 1s;
    -webkit-animation-iteration-count: infinite;
}
#AR{
    -webkit-animation-duration: 2s;
    -webkit-animation-delay: 1s;
    -webkit-animation-iteration-count: infinite;
}
#phone{
    overflow: hidden;
    -webkit-animation-duration: 2s;
    -webkit-animation-delay: 1s;
}
</style>
@section('content')
@extends('nav')
     <div class="container clearfix">
        <div class="row">
            <div class="col-lg-6 col-md-6 pt-5 pb-5 mb-3">
                <h1 class="text-uppercase animated fadeInUp font-weight-bold">Kristel</h1>
                <h4 class="animated fadeInUp">A volunteer booking application for first aid.</h4>
                <h5 class="pt-5 animated fadeIn ">Be a volunteer today!</p>
                 <a id="GS" class="btn btn-rounded btn-blue mt-4 px-5 animated infinite pulse" href="{{url('/register')}}">Get Started <i id="AR" class="fa fa-angle-right animated infinite flash"></i></a>
            </div>
            <div class="col-lg-6 col-md-6">
                <img id="phone" class="animated fadeInDown" src="{{url('img/phone.svg')}}" height="70%" width="100%" style="margin-top:-50px;"/>
            </div>
        </div>

    </div>
    <div class="footer fixed-bottom">
            <div class="text-center">
                <p class="text-uppercase">Kristel 2018 &copy</p>
            </div>
        </div>
@endsection