@extends('master')
<style>
    .container{
        margin-top:70px;
    }
</style>
@section('content')
<div class="progress">
  <div id="pbar" class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<div class="container ">
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Basic Info</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"><button onclick="next(2)" >btn</button></div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"><button onclick="next(1)" >back</button><button onclick="next(3)" >btn</button></div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab"><button onclick="next(2)" >back</button></div>
</div>

</div>
@endsection
<div class="footer fixed-bottom">
    <div class="text-center">
        <p class="text-uppercase">Kristel 2018 &copy</p>
     </div>
</div>

<script>
    function next(i){
     
      switch(i){
          case 1 :   
      
                 document.getElementById("pbar").style.width = "0%"; 
                 var element = document.getElementById("profile-tab");
                 element.classList.add("disabled");  
                 var element2 = document.getElementById("home-tab");
                 element2.classList.remove("disabled");    
                 $('#myTab li:nth-child(' + i +') a').tab('show')
          break;
          case 2 :    document.getElementById("pbar").style.width = "45%"; 
          var element = document.getElementById("profile-tab");
                    element.classList.remove("disabled");  
                    var element2 = document.getElementById("home-tab");
                    element2.classList.add("disabled");    
                    var element3 = document.getElementById("contact-tab");
                    element3.classList.add("disabled");    
                    $('#myTab li:nth-child(' + i +') a').tab('show')
          break;
          case 3 :    document.getElementById("pbar").style.width = "90%"; 
          var element = document.getElementById("contact-tab");
                    element.classList.remove("disabled");  
                    var element2 = document.getElementById("profile-tab");
                    element2.classList.add("disabled");    
                    $('#myTab li:nth-child(' + i +') a').tab('show')
          break;
          default:    document.getElementById("pbar").style.width = "0%"; 
      }
    }
</script>