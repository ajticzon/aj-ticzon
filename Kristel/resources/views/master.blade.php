<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Kristel</title>
        <link rel="stylesheet" href="{{ url('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ url('css/style.css')}}">
        <link rel="stylesheet" href="{{ url('css/animate.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    </head>
    <body>
        @yield('content')
       
      <script type="text/javascript" src="{{ url('js/jquery-3.3.1.min.js')}}"></script>
      <script type="text/javascript" src="{{ url('js/popper.min.js')}}"></script>
      <script type="text/javascript" src="{{ url('js/bootstrap.min.js')}}"></script>
    </body>
</html>
