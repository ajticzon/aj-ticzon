@extends('master')
<style>
    .container{
        margin-top:70px!important;
    }
    .form-control{
        border-radius:100px!important;
    }
    .form-group>label{
        margin-left:5px;
    }  
     #lbl{
        visibility:hidden;
     }
     #lbl2{
         visibility:visible;
     }
     .btn-rounded{
         box-shadow: 0 2px 4px rgba(0,0,0,0.3);
     }
    @media (max-width: 767.98px) {
        .container{
        margin-top:-35px!important;
         }
         #logo{
             height:25%;
         }
         #lbl{
             visibility:visible;
             font-size:1.5em;
         }
         #lbl2{
             visibility:hidden;
             margin-top:-80px;
         }
    }
</style>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt-5">
                <img id="logo" class="animated fadeInDown" src="{{url('img/FullLogo.svg')}}"/><span id="lbl" class="ml-2 animated fadeInDown">Admin Log In</span>
            </div>
            <div class="col-lg-6 6 col-md-6 col-sm-12 col-xs-12 mt-5 animated fadeInUp">
            <h4 id="lbl2" class="mb-3">Admin Log In</h4>
            <form>
                <div class="form-group">
                    <label for="email">Account</label>
                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-md btn-block btn-rounded btn-primary text-uppercase my-4">Log In</button>
                </form>
            </div>
       </div>
    </div>
@endsection
<div class="footer fixed-bottom">
    <div class="text-center">
        <p class="text-uppercase">Kristel 2018 &copy</p>
     </div>
</div>