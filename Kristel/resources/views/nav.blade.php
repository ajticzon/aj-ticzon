<div class="container">
<nav class="navbar navbar-expand-lg navbar-light py-4 animated fadeIn">
<a class="navbar-brand" href="{{url('/')}}"><img class="mx-4" src="{{url('img/logosmall.svg')}}" height="15%"/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse drop-left" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
    <div class="btn-group">
    <a id="linker" class="text-capitalize" href="{{url('/register')}}">register</a>
  </div>
    </ul>
  </div>
</nav>
</div>